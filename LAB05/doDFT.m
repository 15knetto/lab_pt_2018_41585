function [A] = doDFT(x)
%Amp=1.0;f=25;T=0.2;fs=1024;t=0:(1/fs):T-(1/fs);fi=0;
N=max(size(x));
A=zeros(N,1);
for n=1:N
    for k=1:N
        A(k)=A(k)+(x(n)*(cos((2*pi*(-k)*n)/N)+i*sin((2*pi*(-k)*n)/N)));
    end 
end
figure; 
plot(abs(A));
end

