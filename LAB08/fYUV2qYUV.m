function[qY,qU,qV]=fYUV2qYUV( fY, fU, fV, Qy, Quv, c );
qY=round(fY/Qy*c);
qU=round(fU/Quv*c);
qV=round(fV/Quv*c);
end

