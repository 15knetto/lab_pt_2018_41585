function [ rv ] = uYUV2RLE( v )
%% RLE64 (Run-Length Encoding for 64 elements data vector)
% rv - edcoded data (signed int)
% v  - decoded data (must be unsigned int)
% By Cezary Wernik
%% ------------------------------------------------------------------------
   D=0;
   U=1;
   idx=[];
   ix=1;
   idx(ix)=0;
   for i=2:64
      if v(i-1)==v(i)
         if U==1 && ix>1
            idx(ix)=D+1;
            ix=ix+1;
         elseif U==1
            idx(ix)=D;
            ix=ix+1;
         end
         U=U+1;
         D=0;
         if i==64
            idx(ix)=U;
         end
      else
         if D==0 && i>2
            idx(ix)=U;
            ix=ix+1;
         end
         U=1;
         D=D-1;
         if i==64
            idx(ix)=D;
         end
      end
   end
   % scrolling idx, reducing zeros and values  repeating less than 3 times
   idxD=[];
   idxD(1)=0;
   ixD=1;
   for i=1:ix
      if idx(i)<3
         idx(i);
         idxD(ixD)=-abs(idx(i))+idxD(ixD);
      else
         ixD=ixD+1;
         idxD(ixD)=idx(i);
         if i<ix
            ixD=ixD+1;
            idxD(ixD)=0;
         end
      end
   end
   % rewriting output data after computed offsets
   aI=1;
   vI=1;
   rv(aI)=int8(0);
   for i=1:length(idxD)
      rv(aI)=int8(idxD(i));
      if idxD(i)<0
         rv(aI+1:aI+abs(idxD(i)))=int8(v(vI:vI+abs(idxD(i))-1));
         aI=aI+abs(idxD(i))+1;
         vI=vI+abs(idxD(i));
      else
         rv(aI+1)=int8(v(vI));
         aI=aI+2;
         vI=vI+abs(idxD(i));
      end   
   end
end