function[Y,U,V]=rgb2yuv(R,G,B)
Y=0.30*R+0.59*G+0.11*B;
U=0.56*(B-Y)+128;
V=0.71*(R-Y)+128;
end
