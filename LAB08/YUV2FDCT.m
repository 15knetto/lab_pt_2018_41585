function[fY,fU,fV]=YUV2FDCT(Y,U,V)
fY=FDCT(Y);
fU=FDCT(U);
fV=FDCT(V);
end