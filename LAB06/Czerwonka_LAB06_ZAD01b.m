% Wczytanie pliku
[lr,Fs]=audioread('everything-is-fine.wav');

%Wyb�r tylko jednego kana�u
l=lr(:,1);
a=l;
% Obliczenie Fs-punktowej transformaty
L=fft(l,Fs);
b=L;
% Odtworzenie sygna�u l, bez zerowania sk��dowych
%sound(l,Fs)

% Odczekanie 4s
%pause(2);

% Roboczy/pomocniczy wsp�czynnik odci�cia <0,1>
q=0.94;

% Obracanie widma
L=fftshift(L);
c=L;
% Obliczenie progu odci�cia i wyzerowanie kranc�w widma
r=floor((Fs/2)*q);
L(1:r)=0;
L(Fs-r:Fs)=0;
d=abs(L);

% Obracanie widma do pierwotnej kolejno�ci
L=fftshift(L);
e=abs(L);
% Odwrotna transformata
l=real(ifft(L));
f=l;
% Odtworzenie sygna�u l z wyzerowan� cz�ci� widma
sound(l,Fs);
%Wraz ze wzrostem wsp�lczynnik przesuniecia pogarsza sie jakos. slyszalnosc graniczna 0,94
figure;
subplot(3,2,1)
plot(a);
title("Jeden kana�");
xlabel("Czas");
ylabel("Amplituda");
subplot(3,2,2)
plot(abs(b));
title("Fs-punktowa transformata");
xlabel("Cz�stotliwo��");
ylabel("Magnituda");
subplot(3,2,3)
plot(abs(c));
title("Obracanie widma");
xlabel("Cz�stotliwo��");
ylabel("Magnituda");
subplot(3,2,4)
plot(d);
title("Obliczenie progu odci�cia i wyzerowanie kranc�w widma");
xlabel("Cz�stotliwo��");
ylabel("Magnituda");
subplot(3,2,5)
plot(e);
title("Obracanie widma do pierwotnej kolejno�ci");
xlabel("Cz�stotliwo��");
ylabel("Magnituda");
subplot(3,2,6)
plot(f);
title("Odwrotna transformata");
xlabel("Czas");
ylabel("Amplituda");