function[L]=averageCodeLength(codeBook, sortedProbabilityDistribution)
    codeBook=flipud(codeBook);
    L=0;
    for n=1:length(sortedProbabilityDistribution);
        L=L+(length(codeBook{n,2}) * sortedProbabilityDistribution(n));
    end;
end