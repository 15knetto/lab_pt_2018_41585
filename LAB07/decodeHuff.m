function [ msg ] = decodeHuff( codeBook, encodeMsg )
% ************************
% Decode Huffman
%  By Cezary Wernik
% ************************
    mL=0;
    for i=1:size(codeBook,1)
        mtL=length([codeBook{i,2}]);
        if mL < mtL
            mL = mtL;
        end
    end
    codeBook = sortrows(codeBook,[2]);
    
    msg='';
    N=max(size(encodeMsg));
    n=1;
    for l=1:N
        tmpCB={};
        rear=1;
        newTmpCB=codeBook;
        for ml=1:mL;
            for i=1:size(newTmpCB,1)
                if N>=(n+ml-1)
                    nml1=n+ml-1;
                    b=encodeMsg(nml1);
                    if([newTmpCB{i,2}(ml)]==b)
                        tmpCB{rear,1}=newTmpCB{i,1};
                        tmpCB{rear,2}=newTmpCB{i,2};
                        rear=rear+1;
                    end
                end
            end
            if rear==2
                msg=[msg,[tmpCB{1,1}]];
                n=n+ml;
                break;
            end
            newTmpCB=tmpCB;
            tmpCB=[];
            rear=1;
        end
        if n>N % or n>=N :)
            break;
        end
    end
end