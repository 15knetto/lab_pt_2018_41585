function[e]=entropy(x)
 e=-sum(x.*log2(x));
end