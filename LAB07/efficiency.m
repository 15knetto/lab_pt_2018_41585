function[efficiency]=efficiency(H, L)
    efficiency=H/L*100;
end