function[h]=SpoilBit(idx,h)
if(idx==0)
    idx=randi([1 7],1,1)
    h(idx)=~h(idx);
else
    h(idx)=~h(idx);
end
end
