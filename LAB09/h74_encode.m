function[h]=h74_encode(d)
G=[1 1 0 1;
   1 0 1 1;
   1 0 0 0;
   0 1 1 1;
   0 1 0 0;
   0 0 1 0;
   0 0 0 1;
];
h=(G*d);
h=mod(h,2);
end