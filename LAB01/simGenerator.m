function [s]=sinGenerator(A,f,t)
s=A*sin(2*pi*f*t);
end